let creepCommon = {

    // TODO: calculate path on delay s.t. creep isn't constantly shuffling.

    /**
     * Tells creep to harvest the closet source by path.
     *
     * @param {Creep} creep
     */
    harvestClosestSourceByPath: function (creep) {
        let dest = creep.pos.findClosestByPath(FIND_SOURCES);
        if (dest && creep.harvest(dest) === ERR_NOT_IN_RANGE) {
            creep.moveTo(dest, {visualizePathStyle: {stroke: '#ffaa00'}});
        }
    }
};

module.exports = creepCommon;