let roleHarvester = require('role.harvester');
let roleUpgrader = require('role.upgrader');
let roleBuilder = require('role.builder');

// TODO: Roles as classes.
const ROLES = {
    HARVESTER : 'harvester',
    UPGRADER : 'upgrader',
    BUILDER : 'builder'
}

module.exports.loop = function () {
    cleanCreepMemory();
    spawnCreeps('Spawn1');
    runCreeps();
};

function cleanCreepMemory(){
    for(let name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }
}

/**
 * @param {string} spawnName
 */
function spawnCreeps(spawnName) {
    if(!Game.spawns[spawnName]){
        throw new Error(spawnName + ' doesn\'t exist');
    }

    // TODO: Simplify spawning
    // Spawning
    let harvesters = _.filter(Game.creeps, (creep) => creep.memory.role === ROLES.HARVESTER);
    console.log(ROLES.HARVESTER + 's: ' + harvesters.length);

    let upgraders = _.filter(Game.creeps, (creep) => creep.memory.role === ROLES.UPGRADER);
    console.log(ROLES.UPGRADER + 's: ' + upgraders.length);

    let builders = _.filter(Game.creeps, (creep) => creep.memory.role === ROLES.BUILDER);
    console.log(ROLES.BUILDER + 's: ' + builders.length);

    let newName;
    let isSpawning = false;

    if(harvesters.length < 4) {
        newName = ROLES.HARVESTER + Game.time;
        isSpawning = Game.spawns[spawnName].spawnCreep([WORK,WORK,CARRY,MOVE], newName,
            {memory: {role: ROLES.HARVESTER}}) === 0;
    }
    else if(upgraders.length < 4) {
        newName = ROLES.UPGRADER + Game.time;
        isSpawning = Game.spawns[spawnName].spawnCreep([WORK,WORK,CARRY,MOVE], newName,
            {memory: {role: ROLES.UPGRADER}}) === 0;
    }
    else if(Game.spawns[spawnName].room.find(FIND_CONSTRUCTION_SITES).length > 0 && builders.length < 4) {
        newName = ROLES.BUILDER + Game.time;
        isSpawning = Game.spawns[spawnName].spawnCreep([WORK,WORK,CARRY,MOVE], newName,
            {memory: {role: ROLES.BUILDER}}) === 0;
    }

    if(isSpawning) {
        console.log('Spawning: ' + newName);
    }

    if(Game.spawns[spawnName].spawning) {
        let spawningCreep = Game.creeps[Game.spawns[spawnName].spawning.name];
        Game.spawns[spawnName].room.visual.text(
            '🛠️' + spawningCreep.memory.role,
            Game.spawns[spawnName].pos.x + 1,
            Game.spawns[spawnName].pos.y,
            {align: 'left', opacity: 0.8});
    }
}

function runCreeps(){
    // Run creeps
    for(let name in Game.creeps) {
        let creep = Game.creeps[name];

        // TODO: string key -> function
        if(!creep.memory.role){
            throw new Error(creep.name + ": has no role!");
        }

        let creepRunner;

        switch(creep.memory.role){
            case ROLES.HARVESTER:
                creepRunner = roleHarvester.run;
                break;
            case ROLES.UPGRADER:
                creepRunner = roleUpgrader.run;
                break;
            case ROLES.BUILDER:
                creepRunner = roleBuilder.run;
                break;
            default:
                throw new Error(creep.memory.role + ' is invalid!');
                break;
        }

        if(creepRunner){
            creepRunner(creep);
        }
    }
}