let creepCommon = require('creepCommon');

let roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

        // State Determination
	    if(creep.memory.building && creep.carry.energy === 0) {
            creep.memory.building = false;
            creep.say('🔄 harvest');
	    }
	    if(!creep.memory.building && creep.carry.energy === creep.carryCapacity) {
	        creep.memory.building = true;
	        creep.say('🚧 build');
	    }

	    // Actions
	    if(creep.memory.building) {
	        build(creep);
	    }
	    else {
            creepCommon.harvestClosestSourceByPath(creep);
	    }
	}
};

function build(creep) {
    let targets = creep.room.find(FIND_CONSTRUCTION_SITES);
    if(targets.length) {
        if(creep.build(targets[0]) === ERR_NOT_IN_RANGE) {
            creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
        }
    }
    else {
        creep.say('Idle');
    }
}

module.exports = roleBuilder;