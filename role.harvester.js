let creepCommon = require('creepCommon');

// TODO: add role const
// TODO: add build string array

let roleHarvester = {
    /** @param {Creep} creep **/
    run: function(creep) {

        // State determination
        if(!creep.memory.transferring && creep.carry.energy < creep.carryCapacity) {
            creep.memory.harvesting = true;
            creep.say('🔄 harvest');
        }
        else if(creep.carry.energy === creep.carryCapacity){
            creep.memory.harvesting = false;
            creep.memory.transferring = true;
            creep.say('transfer');// TODO: Note this will only appear enroute.
        }
        else if(creep.carry.energy === 0){
            creep.memory.harvesting = true;
            creep.memory.transferring = false;
            creep.say('🔄 harvest');
        }

        // Actions
	    if(creep.memory.harvesting) {
            creepCommon.harvestClosestSourceByPath(creep);
        }
        else if(creep.memory.transferring) {
            transferEnergy(creep);
        }
        else{
            creep.say('Idle!');
        }
	}
};

function transferEnergy(creep){
    let targets = creep.room.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return (structure.structureType === STRUCTURE_EXTENSION || structure.structureType === STRUCTURE_SPAWN) &&
                structure.energy < structure.energyCapacity;
        }
    });
    if(targets.length > 0) {
        if(creep.transfer(targets[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
        }
    }
}

module.exports = roleHarvester;