let creepCommon = require('creepCommon');

let roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {

        // State Determination
        if(creep.memory.upgrading && creep.carry.energy === 0) {
            creep.memory.upgrading = false;
            creep.say('🔄 harvest');
	    }
	    if(!creep.memory.upgrading && creep.carry.energy === creep.carryCapacity) {
	        creep.memory.upgrading = true;
	        creep.say('⚡ upgrade');
	    }

	    // Actions
	    if(creep.memory.upgrading) {
            upgrade(creep);
        }
        else {
            creepCommon.harvestClosestSourceByPath(creep);
        }
	}
};

function upgrade(creep){
    if(creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
        creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}});
    }
}

module.exports = roleUpgrader;